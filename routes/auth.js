var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');

var {verifyUser, verifyPassword, userLookup} = require('../auth');

let SIGNING_KEY = process.env.SIGNING_KEY;

router.post('/login', async function(req, res, next) {
  if(await verifyUser(req.body.username) && await verifyPassword(req.body.username, req.body.password)) {
    let user = userLookup(req.body.username);
    res.json({'jwt': jwt.sign(user.toJSON(), SIGNING_KEY)});
  } else {
    res.status(400);
    res.send({'code': 'USERNAME_UNKNOWN', 'msg': 'Username does not exist.'})
  }  
});

module.exports = router;
