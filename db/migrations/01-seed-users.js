module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('users', [{
            id: 'b46c8ac1-ed9a-48d7-8011-7871adba6316',
            firstName: 'Joshua',
            lastName: 'Hull',
            email: 'joshua.thorild.hull@gmail.com',
            password: '$2b$10$4JLlThaM/4lXssDjr/P9LOuvOtcjIqVuQ8SrSe04BPpX.4cBVQZ/.'
        }], {});
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('users', {email: 'joshua.thorild.hull@gmail.com'});
    }
};