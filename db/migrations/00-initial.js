const Promise = require('bluebird');

module.exports = {
    up: function(query, DataTypes) {
        return query.createTable('users', {
            id: {
                type: DataTypes.UUID,
                allowNull: false,
                defaultValue: DataTypes.UUIDV4
            },
            email: {
                type: DataTypes.TEXT,
                allowNull: false
            },
            password: {
                type: DataTypes.TEXT,
                allowNull: false
            },
            firstName: {
                type: DataTypes.TEXT
            },
            lastName: {
                type: DataTypes.TEXT
            }
        })
    },

    down: function(query, DataTypes) {
        return query.dropTable('users');
    }
}