module.exports = {
    development: {
      dialect: 'sqlite',
      storage: './db.development.sqlite'
    },
    test: {
      username: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_USER,
      host: 'postgres',
      dialect: 'postgres',
      logging: false,
    },
    production: {
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      host: process.env.DB_HOSTNAME,
      dialect: 'postgres'
    }
  };