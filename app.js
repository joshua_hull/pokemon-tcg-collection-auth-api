var express = require('express');
var logger = require('morgan');
var cors = require('cors');

var authRouter = require('./routes/auth');

var corsOptions = {
    origin: function (origin, callback) {
      if (/^http(|s):\/\/.*$/.test(origin)) {
        callback(null, true)
      } else {
        callback(new Error('Not allowed by CORS'))
      }
    }
  }

var env = process.env.NODE_ENV || 'development';
var app = express();

app.use(cors(corsOptions));
if (env === 'production') app.use(logger('dev'));
else if (env === 'development') app.use(logger('dev'))
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/auth', authRouter);

module.exports = app;
