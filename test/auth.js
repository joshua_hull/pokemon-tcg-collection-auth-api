process.env.NODE_ENV='test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let bcrypt = require('bcrypt');
var Umzug = require('umzug');
let server = require('../app');
let should = chai.should();

var models  = require('../db/models');
var {userLookup} = require('../auth');

var umzug = new Umzug({
    storage: 'sequelize',
    storageOptions: {
      sequelize: models.sequelize
    },
    migrations: {
      path: './db/migrations',
      params: [
        models.sequelize.getQueryInterface(),
        models.sequelize.constructor
      ]
    }
  });

chai.use(chaiHttp);

describe('/GET login', () => {
    before(async () => {
        await umzug.up();
        await models.user.create({
            firstName: 'Test',
            lastName: 'User',
            email: 'test.user@example.com',
            password: bcrypt.hashSync('password', 10)
        });
    });

    after(async () => {
        let user = await userLookup('test.user@example.com');
        user.destroy();
        await umzug.down();
    });

    it('it should return a JWT token after logging in', (done) => {
        chai.request(server)
        .post('/auth/login')
        .set('Origin', 'http://localhost:3000')
        .end((err, res) => {
            res.should.have.status(200);
            res.should.have.property('jwt');
            done();
        });
    });
});