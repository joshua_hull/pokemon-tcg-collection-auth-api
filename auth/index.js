var bcrypt = require('bcrypt');
var models  = require('../db/models');
module.exports.verifyUser = (username) => {
    return models.user
    .count({where: { email: username}})
    .then(c => c >= 1);
}

module.exports.verifyPassword = (username, password) => {
    return models.user
    .findOne({where: {email: username}})
    .then(user => bcrypt.compare(password, user.password))
    .then(res => res);
}

module.exports.userLookup = (username) => {
    return models.user
    .findOne({where: {email: username}})
    .then(user => user);
}